﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chess
{
    class Program
    {
        //Horizontal and vertical lenth of the chess board
        public static int size = 8;
        //Definitive list of grid boxes
        public static List<GridBox> gridboxlist = new List<GridBox>();
        public static List<Piece> piecelist = new List<Piece>();
        //Cursor positions
        public static int cursorX = 7;
        public static int cursorY;

        //Value of the GridBox
        public struct GridBox 
        {
            //x and y position for the chess board
            public int x, y;

            //Colour the box will be printed as
            public ConsoleColor color;

            //Colour the character inside the box will be
            public ConsoleColor Charcolor;

            //Character inside the box (" " for blank box)
            public string Content;
        }

        //
        public struct Piece
        {
            public int x, y;

            public bool team;

            public string type;
        }

        //Main function, calls the setup methods then creates an infinant loop
        static void Main(string[] args)
        {
            gridboxlist = GenGridList();
            ColourGridRed();
            DrawGrid();
            while (true)
            {
                KeypressHandler(Console.ReadKey());
                DrawGrid();
            }
        }

        static void KeypressHandler(ConsoleKeyInfo key)
        {
            //Moving the selected box in terms of y
            if(key.Key == ConsoleKey.UpArrow)
            {
                cursorY += 1;
            }
            if (key.Key == ConsoleKey.DownArrow)
            {
                cursorY -= 1;
            }
            //Moving the selected box in terms of x
            if (key.Key == ConsoleKey.LeftArrow)
            {
                cursorX += 1;
            }
            if (key.Key == ConsoleKey.RightArrow)
            {
                cursorX -= 1;
            }
            ColourGridRed();
        }

        public static void ColourGridRed()
        {
            ResetGridColour(gridboxlist);
            for (int i = 0; i < gridboxlist.Count; i++)
            {
                if (gridboxlist[i].x == cursorX)
                {
                    if (gridboxlist[i].y == cursorY)
                    {
                        var x = gridboxlist[i];
                        x.color = ConsoleColor.Red;
                        gridboxlist[i] = x;
                    }
                }
            }
        }

        //Generates a list of coloured blank list boxes
        public static List<GridBox> GenGridList()
        {
            List<GridBox> list = new List<GridBox>();
            for(int row = 0; row < size; row++)
            {
                for (int col = 0; col < size; col++)
                {
                    GridBox i = new GridBox();
                    i.y = row;
                    i.x = col;
                    i.Content = " ";
                    i.color = ConsoleColor.White;
                    list.Add(i);
                }
            }
            ResetGridColour(list);
            
            return list;
        }

        public static List<GridBox> ResetGridColour(List<GridBox> list)
        {
            int i = 0;
            List<GridBox> listn = list;

            for (int row = 0; row < size; row++)
            {
                for (int col = 0; col < size; col++)
                {
                    var itemi = listn[i];
                    itemi.color = ConsoleColor.Black;
                    listn[i] = itemi;

                    if (row % 2 == 0)
                    {
                        if (col % 2 == 0)
                        {
                            var item = listn[i];
                            item.color = ConsoleColor.White;
                            listn[i] = item;
                        }
                    }
                    else
                    {
                        if (col % 2 != 0)
                        {
                            var item = listn[i];
                            item.color = ConsoleColor.White;
                            listn[i] = item;
                        }
                    }
                    i++;
                }
            }
            return listn;
        }

        public static void addpiecestogrid()
        {

        }

        //Draws the grid
        public static void DrawGrid()
        {
            Console.BackgroundColor = ConsoleColor.White;
            Console.Clear();

            //Draws the grid based on the 'gridboxlist' list
            Console.WriteLine(" ");
            for (int i = gridboxlist.Count - 1; 0 <= i; i--)
            {
                if (gridboxlist[i].x == 7)
                {
                    Console.BackgroundColor = ConsoleColor.White;
                    Console.WriteLine();
                    if (cursorY == gridboxlist[i].y)
                    {
                        Console.Write(">");
                    } else
                    {
                        Console.Write(" ");
                    }
                }
                Console.BackgroundColor = gridboxlist[i].color;
                Console.ForegroundColor = gridboxlist[i].Charcolor;

                Console.Write(" " + gridboxlist[i].Content + " ");
            }
            Console.BackgroundColor = ConsoleColor.White;
            Console.WriteLine();
            Console.Write("  ");
            for (int x = 0; x < size; x++)
            {
                if (x == size - cursorX - 1) {
                    Console.Write("^");
                }
                else
                {
                    Console.Write("   ");
                }
            }
        }
    }
}
