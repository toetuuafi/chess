using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Chess2._0
{
    class Program
    {
        public static string[,] board = new string[8,8];
        public static int x, y;
        public static string status;
        public static pieceinfo selected = new pieceinfo();
        public static bool turn = false;

        public struct coord
        {
            public int x, y;
        }

        public struct pieceinfo
        {
            public string piece;
            public int x, y;
        }
        static void Main(string[] args)
        {
            initboard();
            while (true) {
                draw();
                logic(Console.ReadKey());
            }
        }
        //Saves a selected piece to selected
        static void select(int x, int y, string peice)
        {
            selected = new pieceinfo();
            selected.piece = board[y, x];
            selected.x = x;
            selected.y = y;

            status = "Selected:" + board[y, x] + "(" + x + "," + y + ")";
            if (board[y, x] == "   ") status = "Selected: Nothing";
        }

        static void movepiece()
        {
            //Replaces the space in the array with the selected piece
            board[y, x] = board[selected.y, selected.x];
            board[selected.y, selected.x] = "   ";
            selected = new pieceinfo();
            status = "None selected";

            //Swaps turns
            if(turn == false)
            {
                turn = true;
            } else
            {
                turn = false;
            }
        }
        private static void logic(ConsoleKeyInfo key)
        {
            //Cursor Move
            if(key.Key == ConsoleKey.RightArrow) x += 1;
            if(key.Key == ConsoleKey.LeftArrow) x -= 1;
            if(key.Key == ConsoleKey.DownArrow) y += 1;
            if(key.Key == ConsoleKey.UpArrow) y -= 1;

            //Parameters for the cursor
            if (x < 0) x = 0;
            if (y < 0) y = 0;
            if (y > 7) y = 7;
            if (x > 7) x = 7;

            //If enter is pressed
            if (key.Key == ConsoleKey.Enter)
            {
                //If enter is pressed while the cursor is on a peice
                if (board[y, x] != "   ")
                {
                    //Select piece if no other piece is already selected
                    if (selected.piece == "   " || selected.piece == null)
                    {
                        //Selects if the player who's turn it is
                        if ((!turn && board[y, x].ToLower() == board[y, x]) ||
                           (turn && board[y, x].ToUpper() == board[y, x]))
                            select(x, y, board[y, x]);
                    }

                    //Attacking
                    //Checks that the player isnt trying to attack its own piece
                    if ((selected.x != x || selected.y != y) && !(
                        (!turn && board[y, x].ToLower() == board[y, x]) ||
                            (turn && board[y, x].ToUpper() == board[y, x])))
                    {
                        //Pawn attacking
                        if (selected.piece == " p ")
                        {
                            if ((y == selected.y + 1) &&
                                (x == selected.x + 1 || x == selected.x - 1))
                            {
                                movepiece();
                            }
                            else
                            {
                                select(x, y, board[y, x]);
                            }
                        }
                        if (selected.piece == " P ")
                        {
                            if ((x == selected.x + 1 || x == selected.x - 1) &&
                                (y == selected.y - 1))
                            {
                                movepiece();
                            }
                            else
                            {
                                select(x, y, board[y, x]);
                            }
                        }

                        //Knight attacking
                        if (selected.piece == " n " || selected.piece == " N ")
                        {
                            if ((x == selected.x - 1 || x == selected.x + 1 || y == selected.y - 1 || y == selected.y + 1) &&
                                (y == selected.y + 2 || y == selected.y - 2 || x == selected.x + 2 || x == selected.x - 2))
                            {
                                movepiece();
                            }
                            else
                            {
                                select(x, y, board[y, x]);
                            }
                        }

                        //Rook attacking
                        //For each direction that the rook and travel, checks every space for other peices, if there are no other pieces in the way. moves pieces
                        if ((x == selected.x) ^ (y == selected.y))
                        {
                            if (selected.piece == " r " || selected.piece == " R ")
                            {
                                if (!CheckStraightInbetweenPoints()) movepiece();
                            }
                        }

                        //Bishop attacking
                        if (selected.piece == " b " || selected.piece == " B ")
                        {
                            if (!CheckDiagInbetweenPoints()) movepiece();
                        }

                        //Queen attacking
                        if (selected.piece == " q " || selected.piece == " Q ")
                        {
                            if (!CheckDiagInbetweenPoints() || !CheckStraightInbetweenPoints())
                                movepiece();
                        }
                        //King attacking
                        if (selected.piece == " k " || selected.piece == " K ")
                        {
                            if (Math.Abs(x - selected.x) < 2 || Math.Abs(y - selected.y) < 2)
                            {
                                if (!CheckDiagInbetweenPoints() || !CheckStraightInbetweenPoints())
                                    movepiece();
                            }
                        }
                    }
                }
                else
                {
                    if (selected.piece != null)
                    {
                        //Pawn movement parapeters for both sides
                        //Lowercase Pawn
                        if (selected.piece == " p ")
                        {
                            if (x == selected.x)
                            {
                                if (selected.y == 1)
                                {
                                    if (y > 1 && y < 4)
                                    {
                                        movepiece();
                                    }
                                }
                                else
                                {
                                    if (y == selected.y + 1)
                                    {
                                        movepiece();
                                    }
                                }
                            }
                        }
                        //Upper case pawn
                        if (selected.piece == " P ")
                        {
                            if (x == selected.x)
                            {
                                if (selected.y == 6)
                                {
                                    if (y < 6 && y > 3)
                                    {
                                        movepiece();
                                    }
                                }
                                else
                                {
                                    if (y == selected.y - 1)
                                    {
                                        movepiece();
                                    }
                                }
                            }
                        }

                        //Knight Movement
                        if (selected.piece == " n " || selected.piece == " N ")
                        {
                            if (x == selected.x - 1 || x == selected.x + 1 || y == selected.y - 1 || y == selected.y + 1)
                            {
                                if (y == selected.y + 2 || y == selected.y - 2 || x == selected.x + 2 || x == selected.x - 2)
                                {
                                    movepiece();
                                }
                            }
                        }
                        //Rook movement
                        if (selected.piece == " r " || selected.piece == " R ")
                        {
                            if(!CheckStraightInbetweenPoints()) movepiece();
                        }
                        //Bishop Move
                        if (selected.piece == " b " || selected.piece == " B ")
                        {
                            if (!CheckDiagInbetweenPoints()) movepiece();
                        }
                        //Queen movenent
                        if (selected.piece == " q " || selected.piece == " Q ")
                        {
                            if (!CheckDiagInbetweenPoints() || !CheckStraightInbetweenPoints())
                                movepiece();
                        }
                        //King movenent
                        if (selected.piece == " k " || selected.piece == " K ")
                        {
                            if (Math.Abs(x - selected.x) < 2 && Math.Abs(y - selected.y) < 2)
                            {
                                if (!MovingIntoAttack())
                                {
                                    if (!CheckDiagInbetweenPoints() || !CheckStraightInbetweenPoints())
                                        movepiece();
                                }
                            }
                        }
                    }
                    status = "Nothing Selected";
                    selected = new pieceinfo();
                }
            }
        }

        public static bool MovingIntoAttack()
        {
            List<coord> i = ListOfAllPossibleAttacks();
            foreach(coord j in i)
            {
                if (j.x == x && j.y == y) return true;
            }
            return false;
        }

        //Returns as true if there is a piece between the selected space and selected piece
        public static bool CheckDiagInbetweenPoints()
        {
            int diffx = x - selected.x;
            int diffy = y - selected.y;

            if (Math.Abs(diffx) == Math.Abs(diffy))
            {
                //Checks NW
                if (diffx < 0 && diffy < 0)
                {
                    for (int i = 1; i < Math.Abs(diffx); i++)
                    {
                        if (board[-i + selected.y, -i + selected.x] != "   ")
                        {
                            return true;
                        }
                    }
                }
                //Checks SE
                if (diffx > 0 && diffy > 0)
                {
                    for (int i = 1; i < Math.Abs(diffx); i++)
                    {
                        if (board[i + selected.y, i + selected.x] != "   ")
                        {
                            return true;
                        }
                    }
                }
                //Checks SW
                if (diffx > 0 && diffy < 0)
                {
                    for (int i = 1; i < Math.Abs(diffx); i++)
                    {
                        if (board[-i + selected.y, i + selected.x] != "   ")
                        {
                            return true;
                        }
                    }
                }
                //Checks NE
                if (diffx < 0 && diffy > 0)
                {
                    for (int i = 1; i < Math.Abs(diffx); i++)
                    {
                        if (board[i + selected.y, -i + selected.x] != "   ")
                        {
                            return true;
                        }
                    }
                }
                return false;
            }
            return true;
        }

        public static bool CheckStraightInbetweenPoints()
        {
            if ((x == selected.x) ^ (y == selected.y))
            {
                if (x == selected.x && y != selected.y)
                {
                    //Checking down
                    if (y > selected.y)
                    {
                        for (int i = selected.y + 1; i < y; i++)
                        {
                            if (board[i, x] != "   ") return true;
                        }
                    }
                    //Checking Up
                    if (y < selected.y)
                    {
                        for (int i = selected.y - 1; i > -1; i--)
                        {
                            if (board[i, x] != "   ") return true;
                        }
                    }
                }
                if (y == selected.y && x != selected.x)
                {
                    //Checking right
                    if (x > selected.x)
                    {
                        for (int i = selected.x + 1; i < x; i++)
                        {
                            if (board[y, i] != "   ") return true;
                        }
                    }
                    //Checking left
                    if (x < selected.x)
                    {
                        for (int i = selected.x - 1; i > -1; i--)
                        {
                            if (board[y, i] != "   ") return true;
                        }
                    }
                }
                return false;
            }
            return true;
        }

        public static void initboard()
        {
            select(0,0,"   ");
            //Sets the chess board
            board = new string[,]
            { 
                {" r ", " n ", " b ", " q ", " k ", " b ", " n ", " r "}, 
                {" p ", " p ", " p ", " p ", " p ", " p ", " p ", " p "}, 
                {"   ", "   ", "   ", "   ", "   ", "   ", "   ", "   "}, 
                {"   ", "   ", "   ", "   ", "   ", "   ", "   ", "   "}, 
                {"   ", "   ", "   ", "   ", "   ", "   ", "   ", "   "},
                {"   ", "   ", "   ", "   ", "   ", "   ", "   ", "   "},
                {" P ", " P ", " P ", " P ", " P ", " P ", " P ", " P "}, 
                {" R ", " N ", " B ", " Q ", " K ", " B ", " N ", " R "} 
            };
            board = new string[,]
            {
                {"   ", "   ", "   ", "   ", "   ", "   ", "   ", "   "},
                {"   ", "   ", "   ", "   ", "   ", "   ", "   ", "   "},
                {"   ", "   ", "   ", "   ", "   ", "   ", "   ", " k "},
                {"   ", "   ", "   ", "   ", "   ", "   ", "   ", "   "},
                {"   ", "   ", "   ", " K ", "   ", "   ", "   ", "   "},
                {"   ", "   ", "   ", "   ", "   ", "   ", "   ", "   "},
                {"   ", "   ", "   ", "   ", "   ", "   ", "   ", "   "},
                {" r ", "   ", "   ", "   ", "   ", "   ", "   ", "   "},

            };
        }

        public static void draw()
        {
            Console.Clear();

            //Goes through every row and col in the board array and prints each string to the screen
            for(int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    if ((j + i) % 2 == 0)
                    {
                        Console.BackgroundColor = ConsoleColor.White;
                    } else
                    {
                        Console.BackgroundColor = ConsoleColor.Black;
                    }
                    if (j == x && i == y) Console.BackgroundColor = ConsoleColor.Yellow;

                    //Debugging
                    foreach (coord k in ListOfAllPossibleAttacks()) {
                        if (j == k.x && i == k.y)
                        {
                           Console.BackgroundColor = ConsoleColor.Magenta;
                        }
                    }
                
                    Console.ForegroundColor = ConsoleColor.Gray;
                    //Breaks the line
                    if (j == 0) Console.WriteLine();

                    //Selects colour for the pieces to be drawn based on team
                    if(board[i,j].ToUpper() == board[i, j])
                    {
                        Console.ForegroundColor = ConsoleColor.Blue;
                    } else
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                    }
                    Console.Write(board[i, j].ToUpper());
                }
                Console.BackgroundColor = ConsoleColor.Black;

                //Cursor
                //Selects colour for the Cursor to be drawn based on team
                if (turn)
                {
                    Console.ForegroundColor = ConsoleColor.Blue;
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                }

                if (i == y)
                {
                    Console.Write("<");
                }
                else
                {
                    Console.Write(" ");
                }
            }
            //Draws the bottom line with the cursor x position
            Console.BackgroundColor = ConsoleColor.Black;
            string line = " ";
            for (int i = 0; i < x; i++)
            {
                line += "   ";
            }
            line += "^";
            Console.WriteLine();
            Console.WriteLine(line);
            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine(status);
            if (!turn)
            {
                Console.WriteLine("Red Team's Turn");
            } else
            {
                Console.WriteLine("Blue Team's Turn");
            }
        }

        //Complete this
        public static List<coord> ListOfAllPossibleAttacks()
        {
            List<coord> list = new List<coord>();

            for(int x = 0; x < 8; x++)
            {
                for (int y = 0; y < 8; y++)
                {
                    //Kight calculations
                    if ((board[y, x] == " N " && turn == false) || (board[y, x] == " n " && turn == true))
                    {
                        list.Add(newcoord(x - 2, y + 1));
                        list.Add(newcoord(x - 2, y - 1));
                        list.Add(newcoord(x + 2, y + 1));
                        list.Add(newcoord(x + 2, y - 1));

                        list.Add(newcoord(x + 1, y - 2));
                        list.Add(newcoord(x - 1, y - 2));
                        list.Add(newcoord(x + 1, y + 2));
                        list.Add(newcoord(x - 1, y + 2));
                    }

                    //Pawn Calculations
                    if (board[y, x] == " p ")
                    {
                        list.Add(newcoord(x + 1, y + 1));
                        list.Add(newcoord(x - 1, y + 1));
                    }
                    if (board[y, x] == " P ")
                    {
                        list.Add(newcoord(x + 1, y - 1));
                        list.Add(newcoord(x - 1, y - 1));
                    }

                    //Rook and some queen Calculations
                    if ((board[y, x] == " R " && turn == false) || (board[y, x] == " r " && turn == true) ||
                        (board[y, x] == " Q " && turn == false) || (board[y, x] == " q " && turn == true) ||
                            (board[y, x] == " K " && turn == false) || (board[y, x] == " k " && turn == true))
                    {
                        //Up and Down
                        for (int i = y + 1; i < 8; i++)
                        {
                            if (board[y, x].ToLower() == " k " && i - y > 1) break;
                            list.Add(newcoord(x, i));
                            if (board[i, x] != "   ") break;
                        }
                        for (int i = y - 1; i > -1; i--)
                        {
                            if (board[y, x].ToLower() == " k " && -i + y > 1) break;
                            list.Add(newcoord(x, i));
                            if (board[i, x] != "   ") break;
                        }

                        //Left and right
                        for (int i = x + 1; i < 8; i++)
                        {
                            if (board[y, x].ToLower() == " k " && i - x > 1) break;
                            list.Add(newcoord(i, y));
                            if (board[y, i] != "   ") break;
                        }
                        for (int i = x - 1; i > -1; i--)
                        {
                            if (board[y, x].ToLower() == " k " && -i + x > 1) break;
                            list.Add(newcoord(i, y));
                            if (board[y, i] != "   ") break;
                        }
                    }

                    //Bishop and some queen Calculations
                    if ((board[y, x] == " B " && turn == false) || (board[y, x] == " b " && turn == true) ||
                        (board[y, x] == " Q " && turn == false) || (board[y, x] == " q " && turn == true) ||
                        (board[y, x] == " K " && turn == false) || (board[y, x] == " k " && turn == true))

                    {
                        //Checks SE
                        int i = 0;
                        while(true)
                        {
                            i++;
                            if ((board[y, x].ToLower() == " k ") && i < 2) break;

                            if (y + i < 8 && x + i < 8)
                            {
                                list.Add(newcoord(x + i, y + i));
                                if (board[y + i, x + i] != "   ") break;
                            }
                            else break;
                        }

                        //Checks NW
                        i = 0;
                        while (true)
                        {
                            i--;
                            //if (((board[y, x] == " K " && turn == false) || (board[y, x] == " k " && turn == true)) && i > -2) break;
                            if (y + i > - 1 && x + i > -1)
                            {
                                list.Add(newcoord(x + i, y + i));
                                if (board[y + i, x + i] != "   ") break;
                            }
                            else break;
                        }

                        //checks NE
                        i = 0;
                        while (true)
                        {
                            i++;
                            if (((board[y, x] == " K " && turn == false) || (board[y, x] == " k " && turn == true)) &&
                                i < 2) break;
                            if (y - i > -1 && x + i < 8)
                            {
                                list.Add(newcoord(x + i, y - i));
                                if (board[y - i, x + i] != "   ") break;
                            }
                            else break;
                        }

                        //Checks SW
                        i = 0;
                        while (true)
                        {
                            i++;
                            if (((board[y, x] == " K " && turn == false) || (board[y, x] == " k " && turn == true)) &&
                                i < 2) break;
                            if (y + i < 8 && x - i > -1)
                            {
                                list.Add(newcoord(x - i, y + i));
                                if (board[y + i, x - i] != "   ") break;
                            }
                            else break;
                        }
                    }
                }
            }
            return list;
        }

        public static coord newcoord(int x, int y)
        {
            coord i = new coord();
            i.x = x;
            i.y = y;
            return i;
        }
    }
}
